$('#localSearch8').jsLocalSearch({
    "searchinput": "#gsearch8",
    "container": "contsearch8",
    "containersearch": "gsearch",
    "info": true,
});

$(function() {
    var gsearch8 = [
        "Whey",
        "Whey Protein Isolate",
        "Whey Protein Concent.",
        "Basic Whey Protein",
        "100% Pure Whey",
        "BCAA",
        "BCAA Power 3000",
        "Nutrition BCAA 5000",
        "Complexo B",
        "Pré Treino Insanity",
        "Sport Protein Morango",
        "Vital 4k"
    ];
    $("#gsearch8" ).autocomplete({
        source: gsearch8
    });
});